package wz.controller;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import wz.entity.Article;
import wz.entity.Comment;
import wz.entity.User;
import wz.service.ArticleService;
import wz.service.CommentService;
import wz.service.UserService;

@Controller
public class AdminController {
	@Resource  //用这个注解,声明要依赖于业务层的组件
	private UserService userService;
	@Resource
	private ArticleService articleService;
	@Resource
	private CommentService commentService;  
	/**
	 * 登录检测
	 * @author 贾宇奇
	 */
	@RequestMapping("/loginVerify")
	public String login(HttpServletRequest request)
	{
		String username=request.getParameter("username");
		String passwrod=request.getParameter("password");
		String rememberme=request.getParameter("rememberme");
		
		User user= userService.getUserByNameOrEmail(username);
		
		if(user==null) {
			request.setAttribute("msg", "用户名错误");
			return "/login";  
		}
		else if(!user.getUserPass().equals(passwrod)) {
			request.setAttribute("msg", "密码错误");
			return "/login";  
		}
		else {
			//登录成功,把用户信息放到session 合局域中,方便以后使用
			request.getSession().setAttribute("session_user", user);

			//添加cookie 相关的信息 ....
			//更新用户的最后登录时间
			//更新用户的最后登录ip
			/*
			user.setUserLastLoginTime(new Date());
			user.setUserLastLoginIp(request.getRemoteAddr());
			userService.updateUser(user); */
		}
		//首页显示最近5篇文章
		List<Article> articleList =articleService.listRecentAritle(5);
		request.setAttribute("articleList", articleList);

		//首页显示最新的5条评论信息列表
		List<Comment> commentList=commentService.listRecentComment(5);
		request.setAttribute("commentList", commentList);
		// System.out.println(commentList);
		return "/index"; 
	}
	/**
	 * 根据id删除评论
	 * @author 贾宇奇
	 */
	@RequestMapping("/deleteComment")
	public String delete(HttpServletRequest request)
	{
		int id =Integer.parseInt(request.getParameter("commentId"));
		commentService.deleteComment(id);
		List<Article> articleList =articleService.listRecentAritle(5);
		request.setAttribute("articleList", articleList);

		//首页显示最新的5条评论信息列表
		List<Comment> commentList=commentService.listRecentComment(5);
		request.setAttribute("commentList", commentList);
		// System.out.println(commentList);
		return "/index"; 
	
		
	}
}
